'''
Fixtures for tests
'''


README = """# Bitbucket Pipelines Pipe: Trigger Bitbucket Pipelines build

Trigger Bitbucket Pipelines build

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/bitbucket-trigger-pipeline:1.0.0
  variables:
    APP_PASSWORD: $APP_PASSWORD
    REPO: 'your-awesome-repo'
    # ACCOUNT: '<string>' # Optional
    # REF_TYPE: '<string>' # Optional
```
## Variables

```yaml
  - pipe: atlassian/bitbucket-trigger-pipeline:1.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
```
"""

YAML_FRAGMENT = """- pipe: atlassian/bitbucket-trigger-pipeline:1.0.0
  variables:
    APP_PASSWORD: $APP_PASSWORD
    REPO: 'your-awesome-repo'
    # ACCOUNT: '<string>' # Optional
    # REF_TYPE: '<string>' # Optional
"""


PIPE_YML = """
name: Bitbucket trigger pipeline
category: Workflow automation
image: bitbucketpipelines/pipelines-trigger-build:1.0.0
description: Trigger a pipeline in a Bitbucket repository.
repository: https://bitbucket.org/atlassian/bitbucket-trigger-pipeline
maintainer:
    name: Atlassian
    website: https://www.atlassian.com/
tags:
    - pipelines
    - pipes
    - build
    - bitbucket
"""


MANIFEST_YML = """
repositoryPath: 'atlassian/bitbucket-trigger-pipeline'
version: '1.0.0'
"""


PIPE_YML_NO_TAGS = """
name: Bitbucket trigger pipeline
category: Workflow automation
image: bitbucketpipelines/pipelines-trigger-build:1.0.0
description: Trigger a pipeline in a Bitbucket repository.
repository: https://bitbucket.org/atlassian/bitbucket-trigger-pipeline
maintainer:
    name: Atlassian
    website: https://www.atlassian.com/
"""


MANIFEST_YML_CUSTOM = """
name: Bitbucket trigger pipeline
description: Trigger a pipeline in a Bitbucket repository.
category: Workflow automation
logo: my-logo.svg
repositoryPath: 'atlassian/bitbucket-trigger-pipeline'
version: '0.1.0'
maintainer:
  name: Atlassian
  website: https://www.atlassian.com/
yml: |
  - pipe: atlassian/bitbucket-trigger-pipeline:0.1.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
tags:
  - my-tag-1
  - my-tag-2
"""
